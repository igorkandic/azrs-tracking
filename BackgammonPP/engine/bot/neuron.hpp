#pragma once

#include "connectgene.hpp"
#include <vector>
class Neuron
{

  public:
    bool calculated = false;
    std::vector<ConnectGene> incoming;
    double value;
};
