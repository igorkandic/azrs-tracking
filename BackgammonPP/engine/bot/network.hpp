#pragma once
#include "engine/core/boardstate.h"
#include "genome.hpp"
#include "neuron.hpp"
#include <QMap>
#include <QString>
#include <QVector>
#include <fstream>

class Genome;
class Network
{
  public:
    Network(Genome genome);
    Network(std::ifstream &filename);
    double evaluateNetwork(const QVector<double> &inputs);
    const QVector<double> inputFromState(const BoardState &board);

  private:
    void calculateNeuron(Neuron &neuron);
    std::vector<Neuron> neurons;
};
