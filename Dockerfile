FROM ubuntu:latest
COPY . /app
WORKDIR /app
RUN apt -y update && apt -y upgrade
RUN apt-get -y install git cmake ninja-build g++ qt6-base-dev qt6-multimedia-dev libgl-dev
RUN cmake -DBUILD_DOCS=OFF -DBUILD_TESTS=OFF -G Ninja -B build/ -S BackgammonPP/ && ninja -C build/

CMD ["./build/BackgammonPP"]